function getOlderCar(inventory){
    let result = [];
    for (let i=0; i < inventory.length; i++){
        if(inventory[i].car_year < 2000){
            result.push(inventory[i].car_year);
        }
    }
    return result.length;
}

module.exports = getOlderCar;
