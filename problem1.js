function getCarInformation(inventory,car_id){
    for (let i=1; i < inventory.length; i++){
        if (inventory[i].id === car_id){
            return `car ${inventory[i].id} is a ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}`;
        }
    }
}

module.exports = getCarInformation;
