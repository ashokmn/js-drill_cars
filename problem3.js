let inventory = require("./inventory");

function getSortedCarModels(){
    let result =[];
    for (let i=0; i < inventory.length; i++){
        result.push(inventory[i].car_model);
    } 
    for (let i=0;i<result.length-1;i++){
        for (let j=0;j<result.length-1;j++){
            if (result[j].toUpperCase() > result[j+1].toUpperCase()){
                let temp = result[j];
                result[j] = result[j+1];
                result[j+1] = temp;
            }
        }
    }
    return result;
}


module.exports = getSortedCarModels;
