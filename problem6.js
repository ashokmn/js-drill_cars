function bmwOrAudiCars(inventory) {
    let result = [];
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_make === 'BMW' || inventory[i].car_make === 'Audi') {
            result.push(inventory[i]);
        }
    }
    return result;
}


module.exports = bmwOrAudiCars;
