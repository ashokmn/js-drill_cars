const inventory = require("../inventory");
const getOlderCar = require("../problem5");

const expected = 25;

const actual = getOlderCar(inventory);

if (expected === actual){
    console.log("Test passed");
} else {
    console.log('Test failed');
}
