const data = require("../inventory")
const getCarInformation = require("../problem1");

const carId = 33;
const expected = "car 33 is a 2011 Jeep Wrangler";

const actual = getCarInformation(data,carId);

if (actual === expected){
    console.log("Test Passed");
} else {
    console.log("Test Failed");
}
